#ifndef PLANSZA_HH
#define PLANSZA_HH
#define szerokosc_planszy 8
#define dlugosc_planszy 8


#include <iostream>

using namespace std;

class KlasaPlansza {
char pionek_czarny = '@';
char pionek_bialy = '0';
public:
	
    	char Plansza[szerokosc_planszy][dlugosc_planszy];
	
	bool czy_pole_czarne(int i, int j);
	bool gdzie_pionki_biale(int i, int j);
	bool gdzie_pionki_czarne(int i, int j);
	void tworzenie_planszy(char Plansza[szerokosc_planszy][dlugosc_planszy]);
	void rysowanie_planszy(char Plansza[szerokosc_planszy][dlugosc_planszy]);
	void stawianie_pionkow(char Plansza[szerokosc_planszy][dlugosc_planszy]);
	bool czy_koniec_gry(char Plansza[szerokosc_planszy][dlugosc_planszy]);
};

#endif
