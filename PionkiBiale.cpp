#include <iostream>
#include "PionkiBiale.hh"
#define szerokosc_planszy 8
#define dlugosc_planszy 8

using namespace std;


bool KlasaPionekBialy::czy_dobry_pionek_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int &y, int &x)
{		
	bool dobry_pionek = false;
	if(((Plansza[y][x] == pionek_bialy)||(Plansza[y][x] == krolowa_biala)) && ((Plansza[y+1][x-1] == '|') || (Plansza[y+1][x+1] == '|')))
	{	
	
		dobry_pionek = true;
	}
	else
	{
		dobry_pionek = false;
	}
	return dobry_pionek;
}

bool KlasaPionekBialy::czy_mozna_wykonac_ruch_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x)
	{
		bool dobry_ruch = false;
		if(((y+1 == nowa_y) && (x+1 == nowa_x) && (Plansza[nowa_y][nowa_x] == '|')))
		{	
			dobry_ruch = true;
		}
		if(((y+1 == nowa_y) && (x-1 == nowa_x) && (Plansza[nowa_y][nowa_x] == '|')))
		{
			dobry_ruch = true;
		}
		return dobry_ruch;
	}
void KlasaPionekBialy::bicie_wielokrotne(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x)
{
	bool czy_jeszcze_bicie = true;
	do
	{
		if(((Plansza[nowa_y+1][nowa_x+1] == '@') && (Plansza[nowa_y+2][nowa_x+2] == '|')))
		{
			Plansza[nowa_y+1][nowa_x+1] = '|';
			nowa_y = nowa_y+2;
			nowa_x = nowa_x+2;
		}
		if(((Plansza[nowa_y+1][nowa_x-1] == '@') && (Plansza[nowa_y+2][nowa_x-2] == '|')))
		{
			Plansza[nowa_y+1][nowa_x-1] = '|';
			nowa_y = nowa_y+2;
			nowa_x = nowa_x-2;
		}
		else
		{
			czy_jeszcze_bicie = false;
		}
	}while(czy_jeszcze_bicie);
}

bool KlasaPionekBialy::czy_bicie_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x)
{
	bool czy_jest_bicie = false;
	if(Plansza[y][x] == '0')
	{	
		if(nowa_x > x)
		{
			if(((Plansza[nowa_y][nowa_x] == '@') || (Plansza[nowa_y][nowa_x] == '&')) && (Plansza[y+2][x+2] == '|'))   			
			{	
				czy_jest_bicie = true;
    			}
		}
		else
		{		
			if(((Plansza[nowa_y][nowa_x] == '@') || (Plansza[nowa_y][nowa_x] == '&')) && (Plansza[y+2][x-2] == '|'))
	    		{	
				czy_jest_bicie = true;
   			}
		}
	}
	else
	{
		czy_jest_bicie = false;
	}
	return czy_jest_bicie;
}

bool KlasaPionekBialy::czy_mozna_ruszyc_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int &y, int &x, int &nowa_y, int &nowa_x)
{
			bool czy_prawidlowy_ruch = false;
			if((Plansza[nowa_y][nowa_x] == '|'))
			{ 
				if((y > nowa_y) && (x > nowa_x))
				{
					for(int i=1; ((czy_prawidlowy_ruch == false) || ((nowa_y+i == y) && (nowa_x+i == x))); i = i+1)
					{
						if(Plansza[nowa_y+i][nowa_x+i] == '|')
						{
							czy_prawidlowy_ruch = true;
						}
						else
						{
							czy_prawidlowy_ruch = false;
						}
					}
				}
				if((y > nowa_y) && (x < nowa_x))
				{
					for(int i=1; ((czy_prawidlowy_ruch == false) || ((nowa_y+i == y) && (nowa_x-i == x))); i = i-1)
					{
						if(Plansza[nowa_y+i][nowa_x-i] == '|')
						{
							czy_prawidlowy_ruch = true;
						}
						else
						{
							czy_prawidlowy_ruch = false;
						}
					}
				}
				if((y < nowa_y) && (x > nowa_x))
				{
					for(int i=1; ((czy_prawidlowy_ruch == false) || ((nowa_y-i == y) && (nowa_x+i == x))); i = i-1)
					{
						if(Plansza[nowa_y-i][nowa_x+i] == '|')
						{
							czy_prawidlowy_ruch = true;
						}
						else
						{
							czy_prawidlowy_ruch = false;
						}
					}
				}
				if((y < nowa_y) && (x < nowa_x))
				{
					for(int i=1; ((czy_prawidlowy_ruch == false) || ((nowa_y-i == y) && (nowa_y-i == y))); i = i-1)
					{
						if((Plansza[nowa_y-i][nowa_x-i] == '|'))
						{
							czy_prawidlowy_ruch = true;
						}
						else
						{
							czy_prawidlowy_ruch = false;
						}
					}
				}
			}
			return czy_prawidlowy_ruch;
}

bool KlasaPionekBialy::czy_bicie_biala_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x)
{
	bool czy_bicie = false;
	if(Plansza[y][x] == '5')
	{
			if((Plansza[nowa_y][nowa_x] == '@') || (Plansza[nowa_y][nowa_x] == '&'))
			{
				if((y > nowa_y) && (x > nowa_x) && (Plansza[nowa_y-1][nowa_x-1] == '|'))
				{
					for(int i=1; ((czy_bicie == false) || ((nowa_y+i == y) && (nowa_x+i == x))); i = i+1)
					{
				
						if(Plansza[nowa_y+i][nowa_x+i] == '|')
						{
							czy_bicie = true;
						}
						else
						{
							czy_bicie = false;
						}
					}
				}
				if((y > nowa_y) && (x < nowa_x) && (Plansza[nowa_y-1][nowa_x+1] == '|'))
				{
					for(int i=1; ((czy_bicie == false) || ((nowa_y+i == y) && (nowa_x-i == x))); i = i-1)
					{
						if(Plansza[nowa_y+i][nowa_x-i] == '|')
						{
							czy_bicie = true;
						}
						else
						{
							czy_bicie = false;
						}
					}
				}
				if((y < nowa_y) && (x > nowa_x) && (Plansza[nowa_y+1][nowa_x-1] == '|'))
				{
					for(int i=1; ((czy_bicie == false) || ((nowa_y-i == y) && (nowa_x+i == x))); i = i-1)
					{
						if(Plansza[nowa_y-i][nowa_x+i] == '|')
						{
							czy_bicie = true;
						}
						else
						{
							czy_bicie = false;
						}
					}
				}
				if((y < nowa_y) && (x < nowa_x) && (Plansza[nowa_y+1][nowa_x+1] == '|'))
				{
					for(int i=1; ((czy_bicie == false) || ((nowa_y-i == y) && (nowa_y-i == y))); i = i-1)
					{
						if(Plansza[nowa_y-i][nowa_x-i] == '|')
						{
							czy_bicie = true;
						}
						else
						{
							czy_bicie = false;
						}
					}
				}
			}
			return czy_bicie;
	}
	else
	{
		return false;
	}
}
void KlasaPionekBialy::bicie_biala_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x)
{
	if(nowa_y > y)
	{
		if(nowa_x > x)
		{	
			if(((Plansza[nowa_y][nowa_x] == '@') || (Plansza[nowa_y][nowa_x] == '&')) && (Plansza[nowa_y+1][nowa_x+1] == '|'))   				
			{		
				Plansza[nowa_y][nowa_x] = '|';
				nowa_y = nowa_y+1;
				nowa_x = nowa_x+1;
    			}
		}
		else
		{		
			if(((Plansza[nowa_y][nowa_x]  = '@') || (Plansza[nowa_y][nowa_x] == '&')) && (Plansza[nowa_y+1][nowa_x-1] == '|'))
		    	{	
				Plansza[nowa_y][nowa_x] = '|';
				nowa_y = nowa_y+1;
				nowa_x = nowa_x-1;
   			}
		}
	}
	else
	{
		if(nowa_x > x)
		{
			if(((Plansza[nowa_y][nowa_x] == '@') || (Plansza[nowa_y][nowa_x] == '&')) && (Plansza[nowa_y-1][nowa_x+1] == '|'))   				
			{	
				Plansza[nowa_y][nowa_x] = '|';
				nowa_y = nowa_y-1;
				nowa_x = nowa_x+1;
    			}
		}	
		else
		{		
			if(((Plansza[nowa_y][nowa_x]  = '@') || (Plansza[nowa_y][nowa_x] == '&')) && (Plansza[nowa_y-1][nowa_x-1] == '|'))
	    		{	
				Plansza[nowa_y][nowa_x] = '|';
				nowa_y = nowa_y-1;
				nowa_x = nowa_x-1;
   			}
		}
	}
}

void KlasaPionekBialy::bicie_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x)
{
	if(nowa_x > x)
	{
		if((Plansza[nowa_y][nowa_x] == '@') && (Plansza[y+2][x+2] == '|'))   			{	
			Plansza[nowa_y][nowa_x] = '|';
			nowa_y = nowa_y+1;
			nowa_x = nowa_x+1;
    		}
	}
	else
	{		
		if((Plansza[nowa_y][nowa_x]  = '@') && (Plansza[y+2][x-2] == '|'))
	    	{	
			Plansza[nowa_y][nowa_x] = '|';
			nowa_y = nowa_y+1;
			nowa_x = nowa_x-1;
   		}
	}
}

int KlasaPionekBialy::ocenianie_ruchu_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x)
{
	int ocena = 0;
	if(czy_bicie_bialy(Plansza, y, x, nowa_y, nowa_x))
	{
		ocena = ocena + 10;
	}
	if( (nowa_y == 6) || (nowa_y == 7) )
	{
		ocena = ocena + 4;
	}
	if( (nowa_y == 4) || (nowa_y == 5) )
	{
		ocena = ocena + 3;
	}
	if( (nowa_y == 2) || (nowa_y == 3) )
	{
		ocena = ocena + 2;
	}
	if( (nowa_y == 0) || (nowa_y == 1) )
	{
		ocena = ocena + 1;
	}
	if( (nowa_y == 0) || (nowa_x  ==  0) || (nowa_y == 7) || (nowa_x == 7) )
	{
		ocena = ocena + 4;
	}
	if( (nowa_y == 1) || (nowa_x  ==  1) || (nowa_y == 6) || (nowa_x == 6) )
	{
		ocena = ocena + 2;
	}
	return ocena;
}
bool KlasaPionekBialy::czy_to_dobry_ruch_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x)
{
	bool mozliwy_ruch = false;
	bool czy_prawidlowy_pionek = false;
	bool czy_prawidlowy_ruch = false;
	bool czy_bicie = false;	
	if(czy_dobry_pionek_bialy(Plansza, y, x))
	{
		czy_prawidlowy_pionek = true;
	}
	else
	{
		czy_prawidlowy_pionek = false;
	}
	if(czy_mozna_wykonac_ruch_bialy(Plansza, y, x, nowa_y, nowa_x))
	{
		czy_prawidlowy_ruch = true;
	}
	else
	{
		czy_prawidlowy_ruch = false;
	}
	if(czy_bicie_bialy(Plansza, y, x, nowa_y, nowa_x))
	{
		czy_bicie = true;
	}
	else
	{
		czy_bicie = false;
	}
	if(((czy_prawidlowy_pionek == true) && (czy_prawidlowy_ruch == true)) || (czy_bicie == true))
	{
		mozliwy_ruch = true;
	}
	else
	{
		mozliwy_ruch = false;
	}
	return mozliwy_ruch;			
}

void KlasaPionekBialy::ruch_pionkiem_bialym(char Plansza[szerokosc_planszy][dlugosc_planszy])
{
        bool czy_prawidlowy_pionek = false;
        bool czy_prawidlowy_ruch = false;
	bool czy_bicie = false;
        int y, x;
        int nowa_y, nowa_x;
        do
        {
        	cout << "Ktorym pionkiem chcesz wykonac ruch? "; cin >> y; cin >> x;
        	cout << "Gdzie chcesz sie ruszyc? "; cin >> nowa_y; cin >> nowa_x;
		
		if(Plansza[y][x] == '5')
		{
			if(czy_bicie_biala_krolowa(Plansza, y, x, nowa_y, nowa_x))
			{
				bicie_biala_krolowa(Plansza, y, x, nowa_y, nowa_x);
				czy_bicie = true;
			}
			else
			{	
				czy_bicie = false;
			}
/*			if((czy_bicie == true) || (czy_dobry_pionek_bialy(Plansza, y, x)))			
			{
				czy_prawidlowy_pionek = true; 
			}
			else
			{
				czy_prawidlowy_pionek = false;
			}	*/
			if((czy_bicie == true)||(czy_mozna_ruszyc_krolowa(Plansza, y, x, nowa_y, nowa_x))) 
			{
				czy_prawidlowy_ruch = true;
				czy_prawidlowy_pionek = true; 	
			}
			else
			{
				cout << "Tym pionkiem sie nie ruszysz!\n";
				czy_prawidlowy_pionek = false;
			}	
		}
		else
		{
			if(czy_bicie_bialy(Plansza, y, x, nowa_y, nowa_x))
			{
				bicie_bialy(Plansza, y, x, nowa_y, nowa_x);
				bicie_wielokrotne(Plansza, y, x, nowa_y, nowa_x);
				czy_bicie = true;
			}
			else
			{
				czy_bicie = false;
			}		
          	
			if((czy_bicie == true) || (czy_dobry_pionek_bialy(Plansza, y, x)))
	    		{
                		czy_prawidlowy_pionek = true;
            		}
            		else 
	    		{
				cout << "Tym pionkiem sie nie ruszysz!\n";
				czy_prawidlowy_pionek = false;
	    		}
			
			if((czy_bicie == true) || (czy_mozna_wykonac_ruch_bialy(Plansza, y, x, nowa_y, nowa_x)))
            		{ 
                		czy_prawidlowy_ruch = true;
            		}
            		else 
	    		{
				cout << "Nie prawidlowy ruch!\n";
				czy_prawidlowy_ruch = false;
	    		}
		}
	}while((czy_prawidlowy_ruch != true) || (czy_prawidlowy_pionek != true));
	if((nowa_y == 7) || (Plansza[y][x] == krolowa_biala))
	{
		Plansza[nowa_y][nowa_x] = krolowa_biala;
	}
	else
	{
		Plansza[nowa_y][nowa_x] = pionek_bialy;
	}
	Plansza[y][x] = '|';	
}
