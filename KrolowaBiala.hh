#ifndef KROLOWABIALA_HH
#define KROLOWABIALA_HH
#define szerokosc_planszy 8
#define dlugosc_planszy 8

#include <PionkiBiale.hh>
#include <iostream>

using namespace std;

class KlasaKrolowaBiala: public KlasaPionekBialy {
	char krolowa_biala = '5';
	bool czy_koniec_planszy(char Plansza[szerokosc_planszy][dlugosc_planszy], int nowa_wspolrzedna_y, int nowa_wspolrzedna_x);
	bool czy_krolowa_mozna_wykonac_ruch(char Plansza[szerokosc_planszy][dlugosc_planszy], int wspolrzedna_y, int wspolrzedna_x);
	bool czy_mozna_wykonac_ten_ruch_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int wspolrzedna_y, int wspolrzedna_x, int nowa_wspolrzedna_y, int nowa_wspolrzedna_x);	
	
};

#endif
