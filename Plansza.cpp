#include <iostream>
#include "Plansza.hh"
#define szerokosc_planszy 8
#define dlugosc_planszy 8

using namespace std;

bool KlasaPlansza::czy_pole_czarne(int i, int j)
{
	bool CzyPoleCzarne = false;
	if(((i%2==0) && (j%2==0)) || ((i%2!=0)&&(j%2!=0)))
	CzyPoleCzarne = true;
	return CzyPoleCzarne;		
}

bool KlasaPlansza::gdzie_pionki_biale(int i, int j)
{
	bool bialy = false;
	if(((i%2!=0) && (j%2!=0)) || ((i%2==0)&&(j%2==0)))
	bialy = true;
	return bialy;		
}

bool KlasaPlansza::gdzie_pionki_czarne(int i, int j)
{
	bool czarne = false;
	if(((i%2!=0) && (j%2!=0)) || ((i%2==0)&&(j%2==0)))
	czarne = true;
	return czarne;		
}

void KlasaPlansza::tworzenie_planszy(char Plansza[szerokosc_planszy][dlugosc_planszy])
{
	//zmienne i j to zmienne pomocnicze przy przegladaniu i tworzeniu planszy
        int i, j;
        for(i = 0; i < szerokosc_planszy; i++)
        {
            {
                for(j = 0; j < dlugosc_planszy; j++)
                {
                    if(czy_pole_czarne(i,j))
                    {
                        Plansza[i][j]='|';
                    }
                    else
                    {
                        Plansza[i][j]='-';
                    }
                }
            }
        }
}
void KlasaPlansza::stawianie_pionkow(char Plansza[szerokosc_planszy][dlugosc_planszy])
{

	int i,j;
        for(i = 0; i < szerokosc_planszy; i++)
        {
	    //Pionki "0" powinny znajdowac sie tylko w trzech pierwszych rzedach
            if(i<3)
            {
                for(j = 0; j < dlugosc_planszy; j++)
                {
		    //Pionki "0" powinny znajdowac sie tylko na "|"
                    if(gdzie_pionki_biale(i,j))
                    {
                        Plansza[i][j]=pionek_bialy;
                    }
                }
            }
	    //Pionki "@" powinny znajdowac sie tylko w trzech ostatnich rzedach
            if(i>4)
            {
                for(j = 0; j < dlugosc_planszy; j++)
                {
		    //Pionki "@" powinny znajdowac sie tylko na "|"
                    if(czy_pole_czarne(i,j))
                    {
                        Plansza[i][j]=pionek_czarny;
                    }
                }
            }
        }
}
void KlasaPlansza::rysowanie_planszy(char Plansza[szerokosc_planszy][dlugosc_planszy])
{
        int i,j;
        for(i = 0; i < szerokosc_planszy; i++)
        {
            for(j = 0; j < dlugosc_planszy; j++)
            {
		//Wieksze odstepy pomiedzy polami
                cout.width( 2 );
                cout << Plansza[i][j];
            }
            cout << endl;
        }
}

bool KlasaPlansza::czy_koniec_gry(char Plansza[szerokosc_planszy][dlugosc_planszy])
{
	bool koniec = true;
	bool koniec_bialych = true;
	bool koniec_czarnych = true;
	for(int i = 0; i < szerokosc_planszy; i++)
	{
		for(int j = 0; j < dlugosc_planszy; j++)
		{
			if((Plansza[i][j] == '0') || (Plansza[i][j] == '5'))
			{
				koniec_bialych = false;
			}
			if((Plansza[i][j] == '@') || (Plansza[i][j] == '&'))
			{
				koniec_czarnych = false;
			}
		}
	}
	if((koniec_bialych)||(koniec_czarnych))
	{
		return koniec;
	}
	else
	{
		koniec = false;
		return koniec;
	}
}

