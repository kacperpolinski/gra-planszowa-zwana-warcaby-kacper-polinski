#ifndef PIONKICZARNE_HH
#define PIONKICZARNE_HH
#define szerokosc_planszy 8
#define dlugosc_planszy 8

#include <Plansza.hh>
#include <iostream>

using namespace std;

class KlasaPionekCzarny: public KlasaPlansza {
	char pionek_czarny = '@';
	char krolowa_czarna = '&';
	int losowe_pole();
	int W_prawo_czy_w_lewo();
	bool czy_bicie_czarnym(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x);
	void bicie_wielokrotne(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x);
	bool czy_mozna_wykonac_ruch_czarny(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x);	
	bool czy_dobry_pionek_czarny(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x);
	void bicie_czarny(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x);
	bool czy_mozna_ruszyc_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x);
	bool czy_bicie_czarna_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x);
	void bicie_czarna_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x);
	bool czy_to_dobry_ruch(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x);
	int ocenianie_ruchu(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x);
public:
       	void ruch_pionkiem_czarnym(char Plansza[szerokosc_planszy][dlugosc_planszy]);
	void rysuj_plansze(char Plansza[szerokosc_planszy][dlugosc_planszy]);
};

#endif
