#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>
#include "PionkiCzarne.hh"
#include "PionkiBiale.hh"
#include "Ruchy.hh"
#define szerokosc_planszy 8
#define dlugosc_planszy 8

using namespace std;

int KlasaPionekCzarny::losowe_pole()
{
	int indeks; 
	int tablica[] = {0, 1, 2, 3, 4, 5, 6, 7};
	indeks = rand()%szerokosc_planszy;
	return tablica[indeks];
}

int KlasaPionekCzarny::W_prawo_czy_w_lewo()
{
	int indeks;
	int tablica[] = {-1, 1};
	indeks = rand()%2;
	return tablica[indeks];
}



bool KlasaPionekCzarny::czy_dobry_pionek_czarny(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x)
	{
		bool dobry_pionek = false;
		if((Plansza[y][x] == pionek_czarny) && ((Plansza[y-1][x-1] == '|') || (Plansza[y-1][x+1] == '|')))
		{
			dobry_pionek = true;
		}
		else
		{
			dobry_pionek = false;
		}
		return dobry_pionek;
	}

bool KlasaPionekCzarny::czy_mozna_wykonac_ruch_czarny(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x)
	{
		bool dobry_ruch = false;
		if(((Plansza[nowa_y][nowa_x] == Plansza[y-1][x-1]) || (Plansza[nowa_y][nowa_x] == Plansza[y-1][x+1])) && (Plansza[nowa_y][nowa_x] == '|'))
		{
			dobry_ruch = true;
		}
		else
		{
			dobry_ruch = false;
		}
		return dobry_ruch;
	}

void KlasaPionekCzarny::bicie_wielokrotne(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x)
{
	bool czy_jeszcze_bicie = true;
	do
	{
		if(((Plansza[nowa_y-1][nowa_x+1] == '0') && (Plansza[nowa_y-2][nowa_x+2] == '|')))
		{
			Plansza[nowa_y-1][nowa_x+1] = '|';
			nowa_y = nowa_y-2;
			nowa_x = nowa_x+2;
		}
		if(((Plansza[nowa_y-1][nowa_x-1] == '0') && (Plansza[nowa_y-2][nowa_x-2] == '|')))
		{
			Plansza[nowa_y-1][nowa_x-1] = '|';
			nowa_y = nowa_y-2;
			nowa_x = nowa_x-2;
		}
		else
		{
			czy_jeszcze_bicie = false;
		}
	}while(czy_jeszcze_bicie);
}


bool KlasaPionekCzarny::czy_bicie_czarnym(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x)
{
	bool czy_jest_bicie = false;
	if(Plansza[y][x] == '@')
	{
		if(nowa_x > x)
		{
			if(((Plansza[nowa_y][nowa_x] == '0') || (Plansza[nowa_y][nowa_x] == '5')) && (Plansza[y-2][x+2] == '|'))   			
			{	
				czy_jest_bicie = true;
    			}
		}
		else
		{		
			if(((Plansza[nowa_y][nowa_x] == '0') || (Plansza[nowa_y][nowa_x] == '5')) && (Plansza[y-2][x-2] == '|'))
	    		{	
				czy_jest_bicie = true;
   			}
		}
	}
	else
	{
		czy_jest_bicie = false;
	}
	return czy_jest_bicie;
}

void KlasaPionekCzarny::bicie_czarny(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x)
{
	if(nowa_x > x)
	{
		if(((Plansza[nowa_y][nowa_x] == '0')||(Plansza[nowa_y][nowa_x] == '5')) && (Plansza[y-2][x+2] == '|'))   			{	
			Plansza[nowa_y][nowa_x] = '|';
			nowa_y = nowa_y-1;
			nowa_x = nowa_x+1;
    		}
	}
	else
	{		
		if(((Plansza[nowa_y][nowa_x] == '0')||(Plansza[nowa_y][nowa_x] == '5')) && (Plansza[y-2][x-2] == '|'))
	    	{	
			Plansza[nowa_y][nowa_x] = '|';
			nowa_y = nowa_y-1;
			nowa_x = nowa_x-1;
   		}
	}
}

bool KlasaPionekCzarny::czy_mozna_ruszyc_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x)
{
			bool czy_prawidlowy_ruch = false;
			if((Plansza[nowa_y][nowa_x] == '|'))
			{ 
				if((y > nowa_y) && (x > nowa_x))
				{
					for(int i=1; ((czy_prawidlowy_ruch == false) || ((nowa_y+i == y) && (nowa_x+i == x))); i = i+1)
					{
						if(Plansza[nowa_y+i][nowa_x+i] == '|')
						{
							czy_prawidlowy_ruch = true;
						}
						else
						{
							czy_prawidlowy_ruch = false;
						}
					}
				}
				if((y > nowa_y) && (x < nowa_x))
				{
					for(int i=1; ((czy_prawidlowy_ruch == false) || ((nowa_y+i == y) && (nowa_x-i == x))); i = i-1)
					{
						if(Plansza[nowa_y+i][nowa_x-i] == '|')
						{
							czy_prawidlowy_ruch = true;
						}
						else
						{
							czy_prawidlowy_ruch = false;
						}
					}
				}
				if((y < nowa_y) && (x > nowa_x))
				{
					for(int i=1; ((czy_prawidlowy_ruch == false) || ((nowa_y-i == y) && (nowa_x+i == x))); i = i-1)
					{
						if(Plansza[nowa_y-i][nowa_x+i] == '|')
						{
							czy_prawidlowy_ruch = true;
						}
						else
						{
							czy_prawidlowy_ruch = false;
						}
					}
				}
				if((y < nowa_y) && (x < nowa_x))
				{
					for(int i=1; ((czy_prawidlowy_ruch == false) || ((nowa_y-i == y) && (nowa_y-i == y))); i = i-1)
					{
						if((Plansza[nowa_y-i][nowa_x-i] == '|') || Plansza[nowa_y-i][nowa_x-i] == Plansza[y][x])
						{
							czy_prawidlowy_ruch = true;
						}
						else
						{
							czy_prawidlowy_ruch = false;
						}
					}
				}
			}
			return czy_prawidlowy_ruch;
}

bool KlasaPionekCzarny::czy_bicie_czarna_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x)
{
			bool czy_bicie = false;
			if(((Plansza[nowa_y][nowa_x] == '0')||(Plansza[nowa_y][nowa_x] == '5')))
			{
				if((y > nowa_y) && (x > nowa_x) && (Plansza[nowa_y-1][nowa_x-1] == '|'))
				{
					for(int i=1; ((czy_bicie == false) || ((nowa_y+i == y) && (nowa_x+i == x))); i = i+1)
					{
						if(Plansza[nowa_y+i][nowa_x+i] == '|')
						{
							czy_bicie = true;
						}
						else
						{
							czy_bicie = false;
						}
					}
				}
				if((y > nowa_y) && (x < nowa_x) && (Plansza[nowa_y-1][nowa_x+1] == '|'))
				{
					for(int i=1; ((czy_bicie == false) || ((nowa_y+i == y) && (nowa_x-i == x))); i = i-1)
					{
						if(Plansza[nowa_y+i][nowa_x-i] == '|')
						{
							czy_bicie = true;
						}
						else
						{
							czy_bicie = false;
						}
					}
				}
				if((y < nowa_y) && (x > nowa_x) && (Plansza[nowa_y+1][nowa_x-1] == '|'))
				{
					for(int i=1; ((czy_bicie == false) || ((nowa_y-i == y) && (nowa_x+i == x))); i = i-1)
					{
						if(Plansza[nowa_y-i][nowa_x+i] == '|')
						{
							czy_bicie = true;
						}
						else
						{
							czy_bicie = false;
						}
					}
				}
				if((y < nowa_y) && (x < nowa_x) && (Plansza[nowa_y+1][nowa_x+1] == '|'))
				{
					for(int i=1; ((czy_bicie == false) || ((nowa_y-i == y) && (nowa_y-i == y))); i = i-1)
					{
						if(Plansza[nowa_y-i][nowa_x-i] == '|')
						{
							czy_bicie = true;
						}
						else
						{
							czy_bicie = false;
						}
					}
				}
			}
			return czy_bicie;
}

void KlasaPionekCzarny::bicie_czarna_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x)
{
	if(nowa_y > y)
	{
		if(nowa_x > x)
		{	
			if(((Plansza[nowa_y][nowa_x] == '0')||(Plansza[nowa_y][nowa_x] == '5')) && (Plansza[nowa_y+1][nowa_x+1] == '|'))   				
			{		
				Plansza[nowa_y][nowa_x] = '|';
				nowa_y = nowa_y+1;
				nowa_x = nowa_x+1;
    			}
		}
		else
		{		
			if(((Plansza[nowa_y][nowa_x] == '0')||(Plansza[nowa_y][nowa_x] == '5')) && (Plansza[nowa_y+1][nowa_x-1] == '|'))
		    	{	
				Plansza[nowa_y][nowa_x] = '|';
				nowa_y = nowa_y+1;
				nowa_x = nowa_x-1;
   			}
		}
	}
	else
	{
		if(nowa_x > x)
		{
			if(((Plansza[nowa_y][nowa_x] == '0')||(Plansza[nowa_y][nowa_x] == '5')) && (Plansza[nowa_y-1][nowa_x+1] == '|'))   				{	
				Plansza[nowa_y][nowa_x] = '|';
				nowa_y = nowa_y-1;
				nowa_x = nowa_x+1;
    			}
		}	
		else
		{		
			if(((Plansza[nowa_y][nowa_x] == '0')||(Plansza[nowa_y][nowa_x] == '5')) && (Plansza[nowa_y-1][nowa_x-1] == '|'))
	    		{	
				Plansza[nowa_y][nowa_x] = '|';
				nowa_y = nowa_y-1;
				nowa_x = nowa_x-1;
   			}
		}
	}
}

int KlasaPionekCzarny::	ocenianie_ruchu(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x)
{
	int ocena = 0;
	if(czy_bicie_czarnym(Plansza, y, x, nowa_y, nowa_x))
	{
		ocena = ocena + 10;
	}
	if( (nowa_y == 6) || (nowa_y == 7) )
	{
		ocena = ocena + 1;
	}
	if( (nowa_y == 4) || (nowa_y == 5) )
	{
		ocena = ocena + 2;
	}
	if( (nowa_y == 2) || (nowa_y == 3) )
	{
		ocena = ocena + 3;
	}
	if( (nowa_y == 0) || (nowa_y == 1) )
	{
		ocena = ocena + 4;
	}
	if( (nowa_y == 0) || (nowa_x  ==  0) || (nowa_y == 7) || (nowa_x == 7) )
	{
		ocena = ocena + 4;
	}
	if( (nowa_y == 1) || (nowa_x  ==  1) || (nowa_y == 6) || (nowa_x == 6) )
	{
		ocena = ocena + 2;
	}
	return ocena;
}

bool KlasaPionekCzarny::czy_to_dobry_ruch(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x)
{
	bool mozliwy_ruch = false;
	bool czy_prawidlowy_pionek = false;
	bool czy_prawidlowy_ruch = false;
	bool czy_bicie = false;
	if(czy_dobry_pionek_czarny(Plansza, y, x))
	{
		czy_prawidlowy_pionek = true;
	}
	else
	{
		czy_prawidlowy_pionek = false;
	}
	if(czy_mozna_wykonac_ruch_czarny(Plansza, y, x, nowa_y, nowa_x))
	{
		czy_prawidlowy_ruch = true;
	}
	else
	{
		czy_prawidlowy_ruch = false;
	}
	if(czy_bicie_czarnym(Plansza, y, x, nowa_y, nowa_x))
	{
		czy_bicie = true;
	}
	else
	{
		czy_bicie = false;
	}
	if(((czy_prawidlowy_pionek == true) && (czy_prawidlowy_ruch == true)) || (czy_bicie == true))
	{
		mozliwy_ruch = true;
	}
	else
	{
		mozliwy_ruch = false;
	}
	return mozliwy_ruch;			
}

void KlasaPionekCzarny::rysuj_plansze(char Plansza[szerokosc_planszy][dlugosc_planszy])
{
	for(int i = 0; i < szerokosc_planszy; i++)
	{	
		for(int j = 0; j < dlugosc_planszy; j++)
		{
			cout.width(2);
			cout << Plansza[i][j];
		}
		cout << endl;
	}
	cout << endl;
}

void KlasaPionekCzarny::ruch_pionkiem_czarnym(char Plansza[szerokosc_planszy][dlugosc_planszy])
{
	char PlanszaSymulacja[szerokosc_planszy][dlugosc_planszy];
	char PlanszaSymulacjaDruga[szerokosc_planszy][dlugosc_planszy];
	char PlanszaSymulacjaTrzecia[szerokosc_planszy][dlugosc_planszy];
	vector <KlasaRuchy> ListaRuchow;
	vector <KlasaRuchy> ListaOdpPrzeciwnika;
	vector <KlasaRuchy> ListaOdpKomputera;
	bool czy_bicie = false;	
	bool czy_prawidlowy_pionek = false;
	bool czy_prawidlowy_ruch = false;	
	KlasaPionekBialy PionekBialy;
        int y, x;
        int nowa_y, nowa_x;
	int max_ocena = 0, max_2 = 0, max_3 = 0;
	int okolica_x, okolica_y;
	int Tablica[2] = {-1, 1};
	/*Pierwszy Poziom*/
	for(int i = 0; i < szerokosc_planszy; i++)
    	{	
        	for(int j = 0; j < dlugosc_planszy; j++)
        	{
			for(int n = 0; n < 2; n++)
			{
				for(int m = 0; m < 2; m++)
				{
					okolica_y = Tablica[m];
					okolica_x = Tablica[n];
			if(czy_to_dobry_ruch(Plansza, j, i, j+okolica_y, i+okolica_x))
			{		
            			KlasaRuchy *Ruch = new KlasaRuchy;
            			Ruch->x = i;
            			Ruch->y = j;
				Ruch->nowe_x = i+okolica_x;
            			Ruch->nowe_y = j+okolica_y;
				Ruch->ocena = ocenianie_ruchu(Plansza, j, i, j+okolica_y, i+okolica_x);
				for(int a = 0; a < szerokosc_planszy; a++)
				{
					for(int b = 0; b < dlugosc_planszy; b++)
					{
						PlanszaSymulacja[a][b] = Plansza[a][b];
					}
				}
				if((j-1 == 0) || (PlanszaSymulacja[j][i] == krolowa_czarna))
				{
					PlanszaSymulacja[j+okolica_y][i+okolica_x] = krolowa_czarna;
				}
				else
				{
					PlanszaSymulacja[j+okolica_y][i+okolica_x] = pionek_czarny;
				}
				PlanszaSymulacja[j][i] = '|';
				//cout << "Poziom Pierwszy\n";
				//rysuj_plansze(PlanszaSymulacja);	
				/*Drugi Poziom*/	
				for(int c = 0; c < szerokosc_planszy; c++)
    				{	
        				for(int d = 0; d < dlugosc_planszy; d++)
        				{
						int dd = d+1;
						int cc = c+okolica_x;		
						max_3 = 0;						
						if(PionekBialy.czy_to_dobry_ruch_bialy(PlanszaSymulacja, d, c, dd, cc))
						{		
		    					KlasaRuchy *Ruch_bialy = new KlasaRuchy;
		    					Ruch_bialy->x = c;
		    					Ruch_bialy->y = d;
							Ruch_bialy->nowe_x = cc;
		    					Ruch_bialy->nowe_y = dd;
							Ruch_bialy->ocena = PionekBialy.ocenianie_ruchu_bialy(PlanszaSymulacja, d, c, dd, cc);
							//cout << "Druga ocena: " << Ruch_bialy->ocena << endl;
							if(Ruch_bialy->ocena > max_2)
							{
								max_2 = Ruch_bialy->ocena; 
							}
							for(int a = 0; a < szerokosc_planszy; a++)
							{
								for(int b = 0; b < dlugosc_planszy; b++)
								{
									PlanszaSymulacjaDruga[a][b] = PlanszaSymulacja[a][b];
								}
							}
							if((nowa_y == 7) || (PlanszaSymulacjaDruga[d][c] == PionekBialy.krolowa_biala))
							{
								PlanszaSymulacjaDruga[dd][cc] = PionekBialy.krolowa_biala;
							}	
							else
							{
								PlanszaSymulacjaDruga[dd][cc] = PionekBialy.pionek_bialy;
							}
							PlanszaSymulacjaDruga[d][c] = '|';
							//cout << "Poziom Drugi\n";
							//rysuj_plansze(PlanszaSymulacjaDruga);
							/*Trzeci Poziom*/
							for(int e = 0; e < szerokosc_planszy; e++)
    							{	
        							for(int f = 0; f < dlugosc_planszy; f++)
								{
									if(czy_to_dobry_ruch(PlanszaSymulacjaDruga, e, f, e+okolica_y, f+okolica_x))
									{
										KlasaRuchy *Ruch_czarnych = new KlasaRuchy;
		    								Ruch_czarnych->x = f;
		    								Ruch_czarnych->y = e;
										Ruch_czarnych->nowe_x = f+okolica_y;
		    								Ruch_czarnych->nowe_y = e+okolica_x;
							Ruch_czarnych->ocena=ocenianie_ruchu(PlanszaSymulacjaDruga,e,f,e+okolica_y,f+okolica_x);
										//cout << "Trzecia ocena: " << Ruch_czarnych->ocena << endl;
										if(Ruch_czarnych->ocena > max_3)
										{
											max_3 = Ruch_czarnych->ocena; 
										}
										for(int a = 0; a < szerokosc_planszy; a++)
										{
											for(int b = 0; b < dlugosc_planszy; b++)
											{
											PlanszaSymulacjaTrzecia[a][b] = PlanszaSymulacjaDruga[a][b];
											}
										}
										if((nowa_y == 7) || (PlanszaSymulacjaTrzecia[e][f] == krolowa_czarna))
										{
											PlanszaSymulacjaTrzecia[e+okolica_y][f+okolica_x] = krolowa_czarna;
										}	
										else
										{
											PlanszaSymulacjaTrzecia[e+okolica_y][f+okolica_x] = pionek_czarny;
										}
										PlanszaSymulacjaTrzecia[e][f] = '|';
										//cout << "Poziom Trzeci\n";
										//rysuj_plansze(PlanszaSymulacjaTrzecia);
										ListaOdpKomputera.push_back(*Ruch_czarnych);
									}
								}
							}
						//cout << "Max 3: " << max_3 << endl;
						//cout << Ruch_bialy->ocena << " - " << max_3 << endl;
						Ruch_bialy->ocena = Ruch_bialy->ocena - max_3;
						//cout << "Druga ocena " << Ruch_bialy->ocena << endl;
						ListaOdpPrzeciwnika.push_back(*Ruch_bialy);						
						}
					}
				}
		//	cout << "Max 2: " << max_2 << endl;
			Ruch->ocena = Ruch->ocena - max_2;		
			ListaRuchow.push_back(*Ruch);
        		}
			}
			}
    		}
	}	
	y = ListaRuchow[0].y;
	x = ListaRuchow[0].x;
	nowa_y = ListaRuchow[0].nowe_y;
	nowa_x = ListaRuchow[0].nowe_x;
	
	for(int i = 0; i < ListaRuchow.size(); i++)
    	{
		//cout << "y - " << ListaRuchow[i].y <<endl;
       		//cout << "x - " << ListaRuchow[i].x <<endl;			
		//cout << "nowe_y - " << ListaRuchow[i].nowe_y <<endl;
		//cout << "nowe_x - " << ListaRuchow[i].nowe_x <<endl;
		//cout << "ocena ruchu - " << ListaRuchow[i].ocena <<endl;
		//cout << endl;
		if(max_ocena < ListaRuchow[i].ocena)
		{
			max_ocena = ListaRuchow[i].ocena;
			y = ListaRuchow[i].y;
			x = ListaRuchow[i].x;
			nowa_y = ListaRuchow[i].nowe_y;
			nowa_x = ListaRuchow[i].nowe_x;
		}
        }
        do
        {		
		if(Plansza[y][x] == '&')
		{	
			if(czy_bicie_czarna_krolowa(Plansza, y, x, nowa_y, nowa_x))
			{
				bicie_czarna_krolowa(Plansza, y, x, nowa_y, nowa_x);
				czy_bicie = true;
			}
			else
			{	
				czy_bicie = false;
			}	
			if((czy_bicie == true)||((Plansza[y+1][x+1] == '|') || (Plansza[y+1][x-1] == '|') || (Plansza[y-1][x+1] == '|') || (Plansza[y-1][x-1] == '|')))
			{
			czy_prawidlowy_pionek = true; 
			}
			if((czy_bicie == true)||(czy_mozna_ruszyc_krolowa(Plansza, y, x, nowa_y, nowa_x))) 
			{
				czy_prawidlowy_ruch = true;	
			}
			else
			{
				czy_prawidlowy_pionek = false;
			}	
		}
		else
		{
			if(czy_bicie_czarnym(Plansza, y, x, nowa_y, nowa_x))
			{
				bicie_czarny(Plansza, y, x, nowa_y, nowa_x);
				bicie_wielokrotne(Plansza, y, x, nowa_y, nowa_x);				
				czy_bicie = true;
			}		
		    	else
			{
				czy_bicie = false;
			}
			if(((czy_bicie == true) || (czy_dobry_pionek_czarny(Plansza, y,x))))          	
			{
        	    	    czy_prawidlowy_pionek = true;
        	    	}
        		    	else 
		   	{
				czy_prawidlowy_pionek = false;
		    	}
			if((czy_bicie == true) || (czy_mozna_wykonac_ruch_czarny(Plansza, y, x, nowa_y, nowa_x)))
        	    	{ 
        	        	czy_prawidlowy_ruch = true;
        	    	}
        	    	else 
		    	{
				czy_prawidlowy_ruch = false;
		    	}
		}
        }while((czy_prawidlowy_ruch != true) || (czy_prawidlowy_pionek != true));
	if((nowa_y == 0) || (Plansza[y][x] == krolowa_czarna))
	{
		Plansza[nowa_y][nowa_x] = krolowa_czarna;
	}
	else
	{
		Plansza[nowa_y][nowa_x] = pionek_czarny;
	}
	Plansza[y][x] = '|';
}

