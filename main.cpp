#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Plansza.hh"
#include "PionkiBiale.hh"
#include "PionkiCzarne.hh"
#include "Ruchy.hh"
#define szerokosc_planszy 8
#define dlugosc_planszy 8

using namespace std;

int main()
{
	srand( time ( NULL ) );
	KlasaPionekCzarny PionekCzarny;
	KlasaPionekBialy PionekBialy;
	KlasaPlansza Scena;	
	char Plansza[szerokosc_planszy][dlugosc_planszy];
	Scena.tworzenie_planszy(Plansza);
	Scena.stawianie_pionkow(Plansza); 
	cout << "Gra w budowie, by przerwac rozgrywke uzyj ctrl + Z w terminalu\n";
	cout << "W celu wykonania ruchu nalezy najpierw podac wspolrzedne pinonk y spacja x, a nastepnie tak samo pozycje gdzie pionk ma stanac\n";
    	do{
		Scena.rysowanie_planszy(Plansza);
		cout << "Ruch bialych\n"; 
		PionekBialy.ruch_pionkiem_bialym(Plansza);
		Scena.rysowanie_planszy(Plansza);		
		if(Scena.czy_koniec_gry(Plansza))
		{ cout << "Wygral bialy!!!\n"; }
		else
		{
		cout << "Ruch czarnych\n";
        	PionekCzarny.ruch_pionkiem_czarnym(Plansza);	}
		if(Scena.czy_koniec_gry(Plansza))
		{ cout << "Wygral czarny!!!\n"; }	
      	}while(!Scena.czy_koniec_gry(Plansza));
}
