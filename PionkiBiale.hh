#ifndef PIONKIBIALE_HH
#define PIONKIBIALE_HH
#define szerokosc_planszy 8
#define dlugosc_planszy 8

#include <Plansza.hh>
#include <iostream>

using namespace std;


class KlasaPionekBialy: public KlasaPlansza {
public:
	char pionek_bialy = '0';
	char krolowa_biala = '5'; 
	bool czy_bicie_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x);
	bool czy_mozna_wykonac_ruch_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x);
	void bicie_wielokrotne(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x);
	void bicie_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x);
	bool czy_dobry_pionek_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int &y, int &x);
	bool czy_mozna_ruszyc_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int &y, int &x, int &nowa_y, int &nowa_x);
	bool czy_bicie_biala_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x);
	void bicie_biala_krolowa(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int &nowa_y, int &nowa_x);
	void ruch_pionkiem_bialym(char Plansza[szerokosc_planszy][dlugosc_planszy]);
	int ocenianie_ruchu_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x);
	bool czy_to_dobry_ruch_bialy(char Plansza[szerokosc_planszy][dlugosc_planszy], int y, int x, int nowa_y, int nowa_x);
};

#endif
