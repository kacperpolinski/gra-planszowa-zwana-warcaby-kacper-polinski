#
#  To sa opcje dla kompilacji
#
CXXFLAGS=-g -Iinc -Wall -pedantic -std=c++0x

__start__: Warcaby
	./Warcaby

Warcaby: obj obj/main.o obj/Plansza.o obj/PionkiBiale.o obj/PionkiCzarne.o obj/KrolowaBiala.o obj/Ruchy.o
	g++ -Wall -pedantic -std=c++0x -o Warcaby obj/main.o obj/Plansza.o obj/PionkiBiale.o obj/PionkiCzarne.o obj/KrolowaBiala.o obj/Ruchy.o
obj:
	mkdir obj

obj/PionkiCzarne.o: inc/PionkiCzarne.hh src/PionkiCzarne.cpp
	g++ -c ${CXXFLAGS} -o obj/PionkiCzarne.o src/PionkiCzarne.cpp

obj/PionkiBiale.o: inc/PionkiBiale.hh src/PionkiBiale.cpp
	g++ -c ${CXXFLAGS} -o obj/PionkiBiale.o src/PionkiBiale.cpp

obj/KrolowaBiala.o: inc/KrolowaBiala.hh src/KrolowaBiala.cpp
	g++ -c ${CXXFLAGS} -o obj/KrolowaBiala.o src/KrolowaBiala.cpp

obj/Plansza.o: inc/Plansza.hh src/Plansza.cpp
	g++ -c ${CXXFLAGS} -o obj/Plansza.o src/Plansza.cpp

obj/Ruchy.o: inc/Ruchy.hh src/Ruchy.cpp
	g++ -c ${CXXFLAGS} -o obj/Ruchy.o src/Ruchy.cpp

obj/main.o: src/main.cpp inc/Plansza.hh
	g++ -c ${CXXFLAGS} -o obj/main.o src/main.cpp

clean:
	rm -f obj/*.o Warcaby
